import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrialformPage } from './trialform';

@NgModule({
  declarations: [
    TrialformPage,
  ],
  imports: [
    IonicPageModule.forChild(TrialformPage),
  ],
})
export class TrialformPageModule {}
