import { Component , OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators , FormControl } from '@angular/forms';

import { UsernameValidator} from '../trialform/username.validator';

/**
 * Generated class for the TrialformPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-trialform',
  templateUrl: 'trialform.html',
})
export class TrialformPage implements OnInit{

  trialForm: FormGroup;
  submitted = false;
  success =  false;

  validations_form: FormGroup; // username validations instance


  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private formBuilder: FormBuilder ) {  // enter formbuilder module

      this.trialForm = this.formBuilder.group({
        name: new FormControl('',[Validators.required,Validators.minLength(8)]),
        ID: new FormControl('', Validators.compose([                                  // cust validator
          UsernameValidator.validUsername,                                  
          Validators.maxLength(25),
          Validators.minLength(5),
          Validators.pattern('^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$'),
          Validators.required
        ])),
        reason: ['',Validators.required],
        email: new FormControl('',Validators.compose ([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
          ]) )
    });

    this.validations_form = this.formBuilder.group({              
    
    })

  } //const end

 

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrialformPage');
  }

  ngOnInit(){
  }

  onSubmit(){
    this.submitted = true;

      if (this.trialForm.invalid){
        return;
      }
    this.success = true;
  }

}
