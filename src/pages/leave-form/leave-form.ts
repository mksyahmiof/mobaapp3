import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


/**
 * Generated class for the LeaveFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-leave-form',
  templateUrl: 'leave-form.html',
})

export class LeaveFormPage implements OnInit{

  messageForm: FormGroup;
  submitted = false;
  success = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder ) {
   
      this.messageForm = this.formBuilder.group({
        name: ['', Validators.required],
        message: ['', Validators.required]
      })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LeaveFormPage');
  }

  onSubmit(){
    this.submitted = true;

    if (this.messageForm.invalid){
      return;
    }

    this.success = true;
  }

  ngOnInit(){
    }
  
}









/*export class LeaveFormPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LeaveFormPage');
  }
}
*/